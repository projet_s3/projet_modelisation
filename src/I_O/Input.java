package I_O;
import Coordonnes.Coord;
import Coordonnes.Point;
import Coordonnes.Segment;
import Coordonnes.Triangle;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author RYU
 */
public class Input {
   protected String emplacement;
    protected FileReader fichier;
    protected InputStream ips;
   protected InputStreamReader isr;
   protected BufferedReader br;
    protected ArrayList<Point> point;
    protected ArrayList<Segment> segment;
    protected ArrayList<Triangle> faces;
    
    public Input(String emplacement) throws FileNotFoundException{
        this.emplacement = emplacement;
        ips = new FileInputStream(emplacement);
        isr = new InputStreamReader(ips);
        br = new BufferedReader(isr);
        //fichier = new FileReader(emplacement);
        
    }
    
    public void read() throws IOException{
         String ligne="";
        try{
        ligne= br.readLine();
        String[] split = ligne.split(" ");
       int nbPoint = Integer.parseInt(split[0]);
       int nbSegment = Integer.parseInt(split[1]);
       int nbFaces = Integer.parseInt(split[2]);
        point = new ArrayList<Point>();
        String[] split1;
        double x=0;
        double y=0;
        double z=0;
        for(int i=0;i<nbPoint;i++){
            ligne=br.readLine();
            split1=ligne.split(" ");
            x=Double.parseDouble(split1[0]);
            y=Double.parseDouble(split1[1]);
            z=Double.parseDouble(split1[2]);
            point.add(new Point(new Coord(x,y,z)));
        }
        segment = new ArrayList<Segment>();
        String[] split2;
        int a=0;
        int b=0;
        for(int i =0;i<nbSegment;i++){
            ligne = br.readLine();
           split2= ligne.split(" ");
           a = Integer.parseInt(split2[0])-1;
           b= Integer.parseInt(split2[1])-1;
            segment.add(new Segment(point.get(a),point.get(b)));
        }
        String[] split3;
        int face1=0;
        int face2=0;
        int face3=0;
        faces= new ArrayList<Triangle>();
        for (int i=0;i<nbFaces;i++){
            ligne = br.readLine();
           split3= ligne.split(" ");
           face1=Integer.parseInt(split3[0])-1;
           face2=Integer.parseInt(split3[1])-1;
           face3=Integer.parseInt(split3[2])-1;
           faces.add(new Triangle(segment.get(face1), segment.get(face2), segment.get(face3)));
           
        }
       // System.out.println(point);
        //System.out.println(segment);
        //System.out.println(faces);
        }
        catch(Exception e){
           System.out.println(e.getMessage());
        }finally{
              br.close(); 
        }
     
        
    }
    
    public String getEmplacement(){
        return emplacement;
    }
    
    public ArrayList<Point> getPoint(){
        return point;
    }
    
    public ArrayList<Segment> getSegment(){
        return segment;
    }
       
    public ArrayList<Triangle> getTriangle(){
        return faces;
    }
    
    
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException, SQLException{
        Input test = new Input("cube.gts");
        //test.read();
       // BDD bdd = new BDD(test);
        BDD bdd2 = new BDD();
      // bdd.select();
        //bdd.enregistrement("cube2", "cube2","cube");
        bdd2.selectMotCle("1");
    }
}