package Coordonnes;

import java.awt.Color;

public class Matrice {

	int tailleX, tailleY;
	double[][] m;
	Point p1, p2, p3;

	public Matrice(Point p1, Point p2, Point p3) {
		this.tailleX = 4;
		this.tailleY = 3;
		this.m = new double[tailleX][tailleY];
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.m[0][0] = p1.getCoord().getX();
		this.m[1][0] = p1.getCoord().getY();
		this.m[2][0] = p1.getCoord().getZ();
		this.m[0][1] = p2.getCoord().getX();
		this.m[1][1] = p2.getCoord().getY();
		this.m[2][1] = p2.getCoord().getZ();
		this.m[0][2] = p3.getCoord().getX();
		this.m[1][2] = p3.getCoord().getY();
		this.m[2][2] = p3.getCoord().getZ();
		this.m[3][0] = 1;
		this.m[3][1] = 1;
		this.m[3][2] = 1;
	}

	public Matrice(double[][] mat) {
		this.tailleX = mat.length;
		this.tailleY = mat[0].length;
		this.m = new double[tailleX][tailleY];
		for (int x = 0; x < tailleX; x++) {
			for (int y = 0; y < tailleY; y++) {
				this.m[x][y] = mat[x][y];
			}
		}
		this.p1 = null;
		this.p2 = null;
		this.p3 = null;
	}

	public Matrice(Triangle t) {
		this.tailleX = 4;
		this.tailleY = 3;
		this.m = new double[tailleX][tailleY];
		this.p1 = t.getPointX();
		this.p2 = t.getPointY();
		this.p3 = t.getPointZ();
		this.m[0][0] = p1.getCoord().getX();
		this.m[1][0] = p1.getCoord().getY();
		this.m[2][0] = p1.getCoord().getZ();
		this.m[0][1] = p2.getCoord().getX();
		this.m[1][1] = p2.getCoord().getY();
		this.m[2][1] = p2.getCoord().getZ();
		this.m[0][2] = p3.getCoord().getX();
		this.m[1][2] = p3.getCoord().getY();
		this.m[2][2] = p3.getCoord().getZ();
		this.m[3][0] = 1;
		this.m[3][1] = 1;
		this.m[3][2] = 1;
	}

	public Matrice(int nbLignes, int nbColonnes) {
		this.tailleX = nbLignes;
		this.tailleY = nbColonnes;
		this.m = new double[tailleX][tailleY];
		this.p1 = null;
		this.p2 = null;
		this.p3 = null;
	}

	public static Matrice produitMatriciel(Matrice m1, Matrice m2) {
		if (m1.lengthY() == m2.lengthX()) {
			double[][] mat = new double[m1.lengthX()][m2.lengthY()];
			for (int i = 0; i < mat.length; i++) {
				for (int j = 0; j < mat[0].length; j++) {
					for (int cpt = 0; cpt < m1.lengthX(); cpt++) {
						mat[i][j] +=(double) ((m1.m[i][cpt] * m2.m[cpt][j]));
					}
				}
			}
			return new Matrice(mat);
		} else {
			System.out.println("Produit matriciel Imossible");
			System.out.println("Verifiez si les matrices sont compatibles");
			return null;
		}
	}

	public void produitMatriciel(Matrice matrice) {
		if (matrice.lengthX() == this.lengthY()) {
			double[][] mat = new double[this.lengthY()][matrice.lengthY()];
			for (int i = 0; i < mat.length; i++) {
				for (int j = 0; j < mat[0].length; j++) {
					for (int cpt = 0; cpt < matrice.lengthX(); cpt++) {
						mat[i][j] += matrice.m[cpt][j] * this.m[i][cpt];
					}
				}
			}
			this.m = mat;
		} else {
			System.out.println("Produit matriciel Imossible");
			System.out.println("Verifiez si les matrices sont compatibles");
		}
	}
	
	public static Matrice rotationX(Matrice m, double angle){
		double[][] r = new double[4][4];
		angle = radian(angle);
		r[0][0] = 1;
		r[0][1] = 0;
		r[0][2] = 0;
		r[0][3] = 0;
		
		r[1][0] = 0;
		r[1][1] = Math.cos(angle);
		r[1][2] = -Math.sin(angle);
		r[1][3] = 0;
		
		r[2][0] = 0;
		r[2][1] = Math.sin(angle);
		r[2][2] = Math.cos(angle);
		r[2][3] = 0;
		
		r[3][0] = 0;
		r[3][1] = 0;
		r[3][2] = 0;
		r[3][3] = 1;
		
		Matrice rot = new Matrice(r);
		
		return produitMatriciel(rot, m);
	}
	
	public static Matrice rotationY(Matrice m, double angle){
		double[][] r = new double[4][4];
		angle = radian(angle);
		r[0][0] = Math.cos(angle);
		r[0][1] = 0;
		r[0][2] = Math.sin(angle);
		r[0][3] = 0;
		
		r[1][0] = 0;
		r[1][1] = 1;
		r[1][2] = 0;
		r[1][3] = 0;
		
		r[2][0] = -Math.sin(angle);
		r[2][1] = 0;
		r[2][2] = Math.cos(angle);
		r[2][3] = 0;
		
		r[3][0] = 0;
		r[3][1] = 0;
		r[3][2] = 0;
		r[3][3] = 1;
		
		Matrice rot = new Matrice(r);
		
		return produitMatriciel(rot, m);
	}
	
	public static Matrice rotationZ(Matrice m, double angle){
		double[][] r = new double[4][4];
		angle = radian(angle);
		r[0][0] = Math.cos(angle);
		r[0][1] = -Math.sin(angle);
		r[0][2] = 0;
		r[0][3] = 0;
		
		r[1][0] = Math.sin(angle);
		r[1][1] = Math.cos(angle);
		r[1][2] = 0;
		r[1][3] = 0;
		
		r[2][0] = 0;
		r[2][1] = 0;
		r[2][2] = 1;
		r[2][3] = 0;
		
		r[3][0] = 0;
		r[3][1] = 0;
		r[3][2] = 0;
		r[3][3] = 1;
		
		Matrice rot = new Matrice(r);
		
		return produitMatriciel(rot, m);
	}
	
	public Triangle getTriangle(){
		return new Triangle(p1,p2,p3);
	}
	
	public static Matrice translation(Matrice m, Segment s){
		double[][] t = new double[4][4];
		
		t[0][0] = 1;
		t[0][1] = 0;
		t[0][2] = 0;
		t[0][3] = s.getP2().getCoord().getX() - s.getP1().getCoord().getX();
		
		t[1][0] = 0;
		t[1][1] = 1;
		t[1][2] = 0;
		t[1][3] = s.getP2().getCoord().getY() - s.getP1().getCoord().getY();
		
		t[2][0] = 0;
		t[2][1] = 0;
		t[2][2] = 1;
		t[2][3] = s.getP2().getCoord().getZ() - s.getP1().getCoord().getZ();
		
		t[3][0] = 0;
		t[3][1] = 0;
		t[3][2] = 0;
		t[3][3] = 1;
		
		Matrice trans = new Matrice(t);
		
		return produitMatriciel(trans, m);
	}

	public Point getPointX() {
		return new Point(m[0][0], m[1][0], m[2][0]);
	}

	public Point getPointY() {
		return new Point(m[0][1], m[1][1], m[2][1]);
	}

	public Point getPointZ() {
		return new Point(m[0][2], m[1][2], m[2][2]);
	}

	public double[][] getTab() {
		return m;
	}
	
	public void setTab(double[][] m){
		this.m = m;
	}

	public int lengthX() {
		return m.length;
	}

	public int lengthY() {
		return m[0].length;
	}
	
	public static double radian(double angle){
		return angle*(Math.PI/180);
	}
	public static Matrice zoom(Matrice m, double pourcentage){
		double[][] z = new double[4][4];
		double p = 1.0 +(pourcentage/100);
		
		z[0][0] = p;
		z[1][0] = 0;
		z[2][0] = 0;
		z[3][0] = 0;
		
		z[0][1] = 0;
		z[1][1] = p;
		z[2][1] = 0;
		z[3][1] = 0;
		
		z[0][2] = 0;
		z[1][2] = 0;
		z[2][2] = p;
		z[3][2] = 0;
		
		z[0][3] = 0;
		z[1][3] = 0;
		z[2][3] = 0;
		z[3][3] = 1;
		
		Matrice zoom = new Matrice(z);
		
		return produitMatriciel(zoom, m);
	}
	
	//**************************************lumiere
	public static Color getColor(Triangle t) {
        double[] n = produitVectoriel(
                getVecteur(t.getP1(), t.getP2()),
                getVecteur(t.getP2(), t.getP3()));
        double[] v = new double[]{0, 0, 1};

        double coef = Math.abs(Math.cos(((produitScalaire(v, n)) / (normeVectoriel(v) * normeVectoriel(n)))));
        System.out.println("coef = " + coef);
        return new Color((int) (200 * coef), (int) (200 * coef), (int) (200 * coef));
    }
	
	public static double[] produitVectoriel(double[] u, double[] v) {
        double[] res = new double[3];
        res[0] = (u[1] * v[2]) - (u[2] * v[1]);
        res[1] = (u[2] * v[0]) - (u[0] * v[2]);
        res[2] = (u[0] * v[1]) - (u[1] * v[0]);
        return res;
    } 

    public static double produitScalaire(double[] v1, double[] v2) {
        return (v1[0] * v2[0]) + (v1[1] * v2[1]) + (v1[2] * v2[2]);
    }

    public static double normeVectoriel(double[] v) {
        double tmp =0;
    	for(int i = 0; i<3;i++){
        	tmp += v[i]*v[i];
        }
    	return Math.sqrt(tmp);
    }

    public static double[] getVecteur(Point p1, Point p2) {
        double[] res = new double[3];
        res[0] = p2.getCoord().getX()-p1.getCoord().getX();
        res[1] = p2.getCoord().getY()-p1.getCoord().getY();
        res[2] = p2.getCoord().getZ()-p1.getCoord().getZ();
        return res;
    }
    
    //*************************

	public String toString() {
		String result = "";
		for (int x = 0; x < m.length; x++) {
			result += "[";
			for (int y = 0; y < m[0].length; y++) {
				result += m[x][y];
				if (y < m[0].length - 1)
					result += "\t";
			}
			result += "]\n";
		}
		return result;
	}

}
