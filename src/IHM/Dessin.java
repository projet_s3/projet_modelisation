package IHM;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.ArrayList;

import javax.crypto.AEADBadTagException;
import javax.swing.JPanel;

import Coordonnes.Matrice;
import Coordonnes.Point;
import Coordonnes.Triangle;
import Main.Constantes;

public class Dessin extends JPanel {

	ArrayList<Triangle> listModif = new ArrayList<Triangle>();
	double rotationX = 0;
	double rotationY = 0;
	double centrageX = 0;
	double centrageY = 0;
	ArrayList<Triangle> list;

	public Dessin(int x, int y, ArrayList<Triangle> l) {
		this.setSize(x, y);
		this.list = l;
		centrageX = Constantes.taillePanelDessinX / 2
				- Triangle.getCentreFigure(list).getCoord().getX();
		centrageY = Constantes.taillePanelDessinY / 2
				- Triangle.getCentreFigure(list).getCoord().getY();
	}

	public void paint(Graphics g) {
		Point p1 = null;
		Point p2 = null;
		Point p3 = null;
		Graphics2D g2 = (Graphics2D) g;
		for (int i = 0; i < list.size(); i++) {
			try {
				p1 = new Point((int) Constantes.multiplicationPoint
						* (list.get(i).getP1().getCoord().getX()) + centrageX,
						(int) Constantes.multiplicationPoint
								* (list.get(i).getP1().getCoord().getY())
								+ centrageY, (int) list.get(i).getP1()
								.getCoord().getZ());
				p2 = new Point((int) Constantes.multiplicationPoint
						* (list.get(i).getP2().getCoord().getX()) + centrageX,
						(int) Constantes.multiplicationPoint
								* (list.get(i).getP2().getCoord().getY())
								+ centrageY, (int) list.get(i).getP2()
								.getCoord().getZ());
				p3 = new Point((int) Constantes.multiplicationPoint
						* (list.get(i).getP3().getCoord().getX()) + centrageX,
						(int) Constantes.multiplicationPoint
								* (list.get(i).getP3().getCoord().getY())
								+ centrageY, (int) list.get(i).getP3()
								.getCoord().getZ());
				Triangle t = new Triangle(p1, p2, p3);

				drawTriangle(g2, t);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void drawTriangle(Graphics2D g2, Triangle t) throws Exception {
		try {

			Polygon p = new Polygon(t.getTabX(), t.getTabY(), 3);

			g2.setColor(Matrice.getColor(t)); 
			g2.fillPolygon(p);
			g2.drawPolygon(p);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("erreur DrawTriangle");
		}

	}

	public void rotationX(double angle) {

		rotationX = angle;

		Matrice mat1 = null;
		Triangle t1 = null;
		listModif = new ArrayList<Triangle>();
		for (int i = 0; i < list.size(); i++) {

			mat1 = new Matrice(list.get(i).getP1(), list.get(i).getP2(), list
					.get(i).getP3());
			t1 = new Triangle(Matrice.rotationX(mat1, rotationX));

			listModif.add(t1);
		}

		list = listModif;
		this.repaint();
		System.out.println("angle X " + rotationX);
	}

	public static void recentrage(ArrayList<Triangle> l) {
		double centrageEnX = Constantes.taillePanelDessinX / 2
				- Triangle.getCentreFigure(l).getCoord().getX();
		double centrageEnY = Constantes.taillePanelDessinY / 2
				- Triangle.getCentreFigure(l).getCoord().getY();

		for (Triangle i : l) {
			i.getP1().getCoord()
					.setX(i.getP1().getCoord().getX() + centrageEnX);
			i.getP1().getCoord()
					.setY(i.getP1().getCoord().getY() + centrageEnY);

			i.getP2().getCoord()
					.setX(i.getP2().getCoord().getX() + centrageEnX);
			i.getP2().getCoord()
					.setY(i.getP2().getCoord().getY() + centrageEnY);

			i.getP3().getCoord()
					.setX(i.getP3().getCoord().getX() + centrageEnX);
			i.getP3().getCoord()
					.setY(i.getP3().getCoord().getY() + centrageEnY);
		}
	}

	public void rotationY(double angle) {

		rotationY = angle;

		Matrice mat1 = null;
		Triangle t1 = null;
		listModif = new ArrayList<Triangle>();
		for (int i = 0; i < list.size(); i++) {

			mat1 = new Matrice(list.get(i).getP1(), list.get(i).getP2(), list
					.get(i).getP3());
			t1 = new Triangle(Matrice.rotationY(mat1, rotationY));

			listModif.add(t1);
		}

		list = listModif;
		this.repaint();
		System.out.println("angle Y" + rotationY);
	}

	public void centrage() {
		Point centref = Triangle.getCentreFigure(list);
		int recentrageX = (int) (Constantes.taillePanelDessinX / 2 - centref
				.getCoord().getX());
		int recentrageY = (int) (Constantes.taillePanelDessinY / 2 - centref
				.getCoord().getY());
		for (int i = 0; i < list.size(); i++) {
			list.get(i).getP1().getCoord()
					.setX(list.get(i).getP1().getCoord().getX() + recentrageX);
			list.get(i).getP1().getCoord()
					.setY(list.get(i).getP1().getCoord().getY() + recentrageY);
			list.get(i).getP2().getCoord()
					.setX(list.get(i).getP2().getCoord().getX() + recentrageX);
			list.get(i).getP2().getCoord()
					.setY(list.get(i).getP2().getCoord().getY() + recentrageY);
			list.get(i).getP3().getCoord()
					.setX(list.get(i).getP3().getCoord().getX() + recentrageX);
			list.get(i).getP3().getCoord()
					.setY(list.get(i).getP3().getCoord().getY() + recentrageY);
		}

	}

	public void zoom(double z) {

		Matrice mat1 = null;
		Triangle t1 = null;
		listModif = new ArrayList<Triangle>();
		for (int i = 0; i < list.size(); i++) {

			mat1 = new Matrice(list.get(i));
			t1 = new Triangle(Matrice.zoom(mat1, z));

			listModif.add(t1);
		}
		//Dessin.recentrage(listModif);
		list = listModif;
		this.repaint();
		System.out.println("zoom de:" + z + "%");
	}

}
