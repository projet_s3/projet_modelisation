package Coordonnes;

public class Point {


	protected Coord coord;
	
	public Point(Coord c){
		this.coord = c;
	}
	public Point(double x ,double y, double z){
		this.coord = new Coord(x,y,z);
	}
	public void setCoord(Coord c){
		this.coord=c;
	}
	public void setCoord(int x, int y, int z){
		this.coord= new Coord(x,y,z);
		
	}
	
	public Coord getCoord(){
		return this.coord;
	}
        
        public String toString(){
            return coord.toString();
        }
	
}
