package IHM;

import I_O.*;
import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;

import Coordonnes.Matrice;
import Coordonnes.Point;
import Coordonnes.Triangle;

public class ConstructFram extends JFrame {

	public ConstructFram() throws IOException {
		String emplacement = "/home/infoetu/biesbroa/ap/projetS3/cube.gts";
		this.setTitle("Model3D");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(600, 600));
		this.setLocation(50, 50);
		this.setLocationRelativeTo(null);
		this.pack();
		this.setVisible(true);
		ArrayList<Triangle> l = null;
		Input in;
		try { 
			in = new Input(emplacement);
			in.read();
			l = in.getTriangle();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Matrice m1 = null;
		Triangle t1 = null;
		ArrayList<Triangle> listModif = new ArrayList<>();
		for(int i=0;i<l.size();i++){
			
		m1 = new Matrice(l.get(i).getP1(),l.get(i).getP2(),l.get(i).getP3());
		t1 = new Triangle(Matrice.rotationY(m1, 90));
		listModif.add(t1);
		}
		//System.out.println(tpm.get(0).baryCentre().getZ()+ "  " +tpm.get(1).baryCentre().getZ() );
		this.add(new Dessin(this.getWidth(), this.getHeight(), listModif));
		System.out.println(Triangle.getCentreFigure(l));
	}

	public static ArrayList<Triangle> ordoPaint(ArrayList<Triangle> t) {
		Triangle tmp = t.get(0);
		ArrayList<Triangle> j = new ArrayList<Triangle>();
		while (t.isEmpty()) {
			for (int y = 1; y < t.size(); y++) {
				if (tmp.baryCentre().getZ() < t.get(y).baryCentre().getZ()) {
					tmp = t.get(y);
				}
			}
			t.remove(tmp);
			j.add(tmp);
		}

		return j;
	}
}
