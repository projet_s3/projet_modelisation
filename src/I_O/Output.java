package I_O;
/**
 *
 * @author RYU
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
public class Output {
    
    protected FileWriter fw;
    protected BufferedWriter bfw;
    protected Input input;
    protected String emplacement;
    public Output(Input i, String emplacement){
        this.input=i;
        this.emplacement=emplacement;
    }
    
    private String read() throws IOException{
        String write="";
        try{
        InputStream ips = new FileInputStream(input.getEmplacement());
        InputStreamReader isr = new InputStreamReader(ips);
       BufferedReader br = new BufferedReader(isr);
       write =br.readLine();
       String[] split = write.split(" ");
       int nbPoint = Integer.parseInt(split[0]);
       int nbSegment = Integer.parseInt(split[1]);
       int nbFaces = Integer.parseInt(split[2]);
       write+="\n";
       int nbLigne= nbPoint+nbSegment+nbFaces;
       for (int i=0;i<nbLigne;i++){
           write+=br.readLine()+"\n";
       }
        }
        catch(Exception e){
            e.getMessage();
        }
                   System.out.println(write);
                   return write;
        
    }
    
    public void write() throws IOException{
        try{
        fw = new FileWriter(emplacement, false);
        bfw = new BufferedWriter(fw);
        bfw.write(this.read());
        bfw.flush();
        bfw.close();
        
        }
        
        catch(Exception e){
            e.getMessage();
        }
    }
    
    
}