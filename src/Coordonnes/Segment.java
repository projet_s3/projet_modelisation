package Coordonnes;

public class Segment {
 protected Point p1;
 protected Point p2;
 
 public Segment(Point j, Point k){
	 this.p1 = j;
	 this.p2 = k;
 }

public Point getP1() {
	return p1;
}

public void setP1(Point p1) {
	this.p1 = p1;
}

public Point getP2() {
	return p2;
}

public void setP2(Point p2) {
	this.p2 = p2;
}
public String toString(){
    return "{"+p1.toString()+", "+p2.toString()+"}";
}
 
 
	
}
