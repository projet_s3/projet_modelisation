package IHM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class OpenMenu extends JFrame implements ActionListener{
	
	JLabel l1 = new JLabel();
	
	String file = "";
	
	JButton b1 = new JButton();
	JButton bHelp = new JButton();
	JButton bOption = new JButton();
	
	JDialog Opt = new JDialog(this, "A venir");
	
	JFileChooser filec = new JFileChooser();
	
	JPanel p1 = new JPanel();
	
	public OpenMenu(){
		
		this.setTitle("Acceuil Mod3D");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(400, 200));
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout(50, 50));
		this.setResizable(false);
		this.pack();
		this.setVisible(true);
		
		GridLayout gl = new GridLayout(0, 8, 5, 5);
		p1.setLayout(gl);
		
		Opt.setBounds(850, 500, 170, 80);
		
		l1.setText("Modelisation 3D");
		this.getContentPane().add(l1, BorderLayout.NORTH);

		b1.setText("Ouvrir un modele");
		b1.setPreferredSize(new Dimension(200, 50));
		this.getContentPane().add(b1, BorderLayout.CENTER);
		b1.addActionListener(this);

		this.getContentPane().add(p1, BorderLayout.SOUTH);
		this.getContentPane().add(new JPanel(), BorderLayout.WEST);
		this.getContentPane().add(new JPanel(), BorderLayout.EAST);

		bHelp.setText("?");
		bHelp.addActionListener(this);
		p1.add(bHelp);
		
		bOption.setText("O");
		bOption.addActionListener(this);
		p1.add(bOption);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==b1) { 
			
			int retour = filec.showOpenDialog(OpenMenu.this);
            
	          if(retour==JFileChooser.APPROVE_OPTION){
	        	  
	        	  System.out.println("event");
	        	 
	        	   file = filec.getSelectedFile().getName();
	        	   
	        	   this.setVisible(false);
	        		
	        	   try {
					new FirstFenetre(file).setVisible(true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	        	   
	        	   this.repaint();
	        	   
	        	   filec.getSelectedFile().getAbsolutePath();
	        	   
	          }else{
	        	  
	        	  System.out.println("error");
	          }
	
		}
		
		//Temporaire
		if(e.getSource()==bHelp) { 
			
			Opt.setVisible(true); 
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Opt.setVisible(false);
			
		}

		//Temporaire
		if(e.getSource()==bOption) { 
	
			Opt.setVisible(true);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Opt.setVisible(false);
		}
		
	}
}