package IHM;

import I_O.BDD;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class FenetreRecupBdd extends JFrame implements ActionListener {
    
        BDD bdd;
	
	String selected = "testouille";
	String mot = "motclé";
	
	final JTable table;
	
	JLabel debug;
	JLabel rechercheTitle;
	
	JTextField recherche;
	
	JButton valider;
	JButton annuler;
        DefaultTableModel table1;
	
	final TableRowSorter<TableModel> sorter;
        
        ArrayList<String> nom;
        ArrayList<String> type;
        ArrayList<String> description;
	
	public FenetreRecupBdd() throws SQLException, ClassNotFoundException{
            
            bdd = new BDD();
            bdd.selectMotCle(" ");
            
           nom= bdd.getNom();
           type= bdd.getType();
           description= bdd.getDescription();
		debug = new JLabel("test");
		
		this.setTitle("Import");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(700, 175));
		this.setLocation(100, 100);
		this.setLayout(null);
		this.setResizable(false);
		this.pack();
		this.setVisible(true);
		
		String[] columnNames = {"Nom",
                "Type",
                "Description"};
                
		Object[][] data = {};
                

		 table1 = new DefaultTableModel(data, columnNames){
			public boolean isCellEditable(int row, int col) {
				return false;
			}};
                 
		
		sorter = new TableRowSorter<TableModel>(table1);
		table = new JTable(table1);
		table.setRowSorter(sorter);
			
		table.setPreferredScrollableViewportSize(new Dimension(300, 70));
		table.setFillsViewportHeight(true);
		JScrollPane scrollPane = new JScrollPane(table);
		table.changeSelection(1, 3, false, false);
		table.setDragEnabled(false);
		table.setEditingColumn(0);
		table.setEditingRow(0);
		table.setSelectionMode(0);
		scrollPane.setBounds(0, 0,500, 275);
		this.add(scrollPane);
		
		rechercheTitle = new JLabel();
		rechercheTitle.setText("Mot(s) clé(s) :");
		rechercheTitle.setBounds(525, 15, 120, 13);
		this.add(rechercheTitle);
		
		recherche = new JTextField();
		recherche.addActionListener(this);
		recherche.setBounds(525, 30, 150, 25);
		this.add(recherche);
		
		valider = new JButton();
		valider.setText("Vailder");
		valider.setBounds(550, 65, 100, 35);
		valider.addActionListener(this);
		this.add(valider);
		
		annuler = new JButton();
		annuler.setText("Annuler");
		annuler.setBounds(550, 105, 100, 35);
		annuler.addActionListener(this);
		this.add(annuler);
		
		//tests
                for(int i=0;i<nom.size();i++)
                    table1.addRow(new String[]{nom.get(i),type.get(i), description.get(i)});
                
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if(e.getSource().equals(valider)){
			
			
			int ligne = table.getSelectedRow();
			
			selected = (String) table.getValueAt(ligne, 0);
			
			System.out.println(selected);
			this.setVisible(false);
                        
                        
					
		}

		if(e.getSource().equals(annuler)){
                    
                        this.dispose();
			
			this.setVisible(false);
			
		}
		
		if(e.getSource().equals(recherche)){
                    if(table1.getRowCount()>0)
			supprimerRow();
                    
			mot = recherche.getText();
			
                    
                    try {
                        bdd.selectMotCle(recherche.getText());
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FenetreRecupBdd.class.getName()).log(Level.SEVERE, null, ex);
                    }
                  
                    
                    
                    nom.clear();
                    type.clear();
                    description.clear();
                
                    nom=new ArrayList<>(bdd.getNom());
                    type= new ArrayList<>(bdd.getType());
                    description= new ArrayList<>(bdd.getType());
                    

                    System.out.println(bdd.getNom());
                    
                    for(int i=0;i<nom.size();i++)
                    table1.addRow(new String[]{nom.get(i),type.get(i), description.get(i)});
                    
                    
			System.out.println(mot);
		}
	}
        
        private void supprimerRow(){   
            table1.removeRow(0);
            if(table1.getRowCount()>0)
                supprimerRow();
        }


}
