package IHM;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Coordonnes.Matrice;
import Coordonnes.Triangle;
import I_O.BDD;
import I_O.Input;
import Main.Constantes;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;



public class FirstFenetre extends JFrame implements ActionListener,
		MouseWheelListener {
	
	ArrayList<Triangle> listTriangle = new ArrayList<Triangle>();
	Dessin dessin;
	
	int constX = 0;
	int constY = 0;

	double rotationY = 0;
	
	String file = "cube.gts";

	MenuBar mbar = new MenuBar();
	Menu m1 = new Menu("Fichier");
	Menu m2 = new Menu("Aide");
	Menu m3 = new Menu("Options");
	MenuItem item1 = new MenuItem("Charger");
	MenuItem item2 = new MenuItem("Sauver");
	MenuItem item3 = new MenuItem("Exporter");
	MenuItem item4 = new MenuItem("Rechercher");
	MenuItem item5 = new MenuItem("Aide");
	MenuItem item6 = new MenuItem("Options");

	JPanel p = new JPanel();

	JPanel deplacement = new JPanel();
	JPanel zoom = new JPanel();

	JButton bLight = new JButton();
	JButton fh1 = new JButton();
	JButton fb1 = new JButton();
	JButton fg1 = new JButton();
	JButton fd1 = new JButton();
	JButton fh2 = new JButton();
	JButton fb2 = new JButton();
	JButton fg2 = new JButton();
	JButton fd2 = new JButton();
	JButton z1 = new JButton();
	JButton z2 = new JButton();

	JLabel depl = new JLabel();
	JLabel zoomeuh = new JLabel();
	JLabel modele = null;
	JLabel percent = new JLabel();

	JTextField saisie = new JTextField();
	
	JFileChooser filec = new JFileChooser();
	
	Input i;

	public FirstFenetre(String file) throws IOException {
		
		i = new Input(file);
		
		// test dessin
		
		i.read();
		listTriangle = i.getTriangle();

		dessin = new Dessin(Constantes.taillePanelDessinX,
				Constantes.taillePanelDessinY, listTriangle);
		
		p = dessin;
		p.addMouseWheelListener(this);

		//
		this.setTitle("ModÃ©lisation 3D");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(1000, 800));
		// this.setLocation(5, 5);
		this.setLayout(null);
		this.setResizable(false);
		this.pack();
		this.setVisible(true);

		// textfield
		saisie.setBounds(185, 135, 45, 45);
		saisie.setText("0");
		zoom.add(saisie);

		// barre de menu
		this.setMenuBar(mbar);
		mbar.add(m1);
		mbar.add(m2);
		mbar.add(m3);
		m1.add(item1);
		item1.addActionListener(this);
		m1.add(item4);
		item4.addActionListener(this);
		m1.add(item2);
		item2.addActionListener(this);
		m1.add(item3);
		item3.addActionListener(this);
		m2.add(item5);
		item5.addActionListener(this);
		m3.add(item6);
		item6.addActionListener(this);

		// les panels

		this.add(p);
		deplacement.setBounds(630, 20, 350, 325);
		deplacement.setBackground(Color.LIGHT_GRAY);
		this.add(deplacement);
		zoom.setBounds(630, 375, 350, 325);
		zoom.setBackground(Color.LIGHT_GRAY);
		this.add(zoom);

		// les boutons
		bLight.setText("Source de lumiÃ¨re");
		bLight.setBounds(640, 710, 300, 50);
		this.add(bLight);
		fh1.setText("â†‘");
		fb1.setText("â†“");
		fg1.setText("â†");
		fd1.setText("â†’");
		fh2.setText("â†‘");
		fb2.setText("â†“");
		fg2.setText("â†");
		fd2.setText("â†’");
		z1.setText("+");
		z2.setText("-");

		fh1.addActionListener(this);
		fg1.addActionListener(this);
		fd1.addActionListener(this);
		fb1.addActionListener(this);
		fh2.addActionListener(this);
		fg2.addActionListener(this);
		fd2.addActionListener(this);
		fb2.addActionListener(this);
		z1.addActionListener(this);
		z2.addActionListener(this);

		fh1.setBounds(75, 90, 45, 45);
		fg1.setBounds(30, 135, 45, 45);
		fd1.setBounds(120, 135, 45, 45);
		fb1.setBounds(75, 180, 45, 45);
		deplacement.add(fh1);
		deplacement.add(fg1);
		deplacement.add(fb1);
		deplacement.add(fd1);

		fh2.setBounds(230, 90, 45, 45);
		fg2.setBounds(185, 135, 45, 45);
		fd2.setBounds(275, 135, 45, 45);
		fb2.setBounds(230, 180, 45, 45);
		deplacement.add(fh2);
		deplacement.add(fg2);
		deplacement.add(fb2);
		deplacement.add(fd2);

		z1.setBounds(110, 90, 45, 45);
		z2.setBounds(110, 180, 45, 45);
		zoom.add(z1);
		zoom.add(z2);

		// les labels
		p.setLayout(null);
		deplacement.setLayout(null);
		zoom.setLayout(null);
		depl.setText("DEPLACEMENT / ROTATION");
		depl.setBounds(9, 10, 200, 20);
		deplacement.add(depl);
		zoomeuh.setText("ZOOM");
		zoomeuh.setBounds(9, 10, 200, 20);
		zoom.add(zoomeuh);

		percent.setText("%");
		percent.setBounds(235, 135, 45, 45);
		zoom.add(percent);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// ZOOM +
		if (e.getSource().equals(z1)) {

			if (saisie.getText().isEmpty())
				saisie.setText("0");

			int tmp = Integer.valueOf(saisie.getText());
			tmp += 5;
			saisie.setText(String.valueOf(tmp));

			dessin.zoom(5);
			this.repaint();

		}

		// ZOOM -
		if (e.getSource().equals(z2)) {

			if (saisie.getText().isEmpty())
				saisie.setText("0");

			int tmp = Integer.valueOf(saisie.getText());
			saisie.setText(String.valueOf(tmp - 5));

			dessin.zoom(-5);
			this.repaint();
		}

		if (e.getSource().equals(fb2)) {
			dessin.rotationX(-36);
			this.repaint();
		}

		if (e.getSource().equals(fh2)) {
			dessin.rotationX(36);
			this.repaint();
		}

		if (e.getSource().equals(fg2)) {
			dessin.rotationY(36);
			this.repaint();
		}
		if (e.getSource().equals(fd2)) {
			dessin.rotationY(-36);
			this.repaint();
		}
		
		if (e.getSource().equals(item1)) {
			
	          int retour = filec.showOpenDialog(FirstFenetre.this);
	            
	          if(retour==JFileChooser.APPROVE_OPTION){
	        	  
	        	  System.out.println("event");
	        	 
	        	   file = filec.getSelectedFile().getName();
	        	   
	        	   
	        	   this.setVisible(false);
	        		
	        	   try {
					new FirstFenetre(file).setVisible(true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	        	   
	        	   this.repaint();
	        	   
	        	   filec.getSelectedFile().getAbsolutePath();
	        	   
	          }else{
	        	  
	        	  System.out.println("error");
	          }
	          
		}
		
		if (e.getSource().equals(item2)) {
			
			FenetreEnreBase fe = new FenetreEnreBase(new BDD(i));
			fe.setVisible(true);
			
		}
                
                if (e.getSource().equals(item4)) {

                    FenetreRecupBdd f= null;
                    try {
                        f = new FenetreRecupBdd();
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(FirstFenetre.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    f.setVisible(true);

}
		
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		double zoom = e.getPreciseWheelRotation();
		dessin.zoom(zoom*3);
		this.repaint();

	}
	
	/* yÃ© sÃ© pas comment on fait.
	 
	public void mouseDragged(MouseEvent e) {
	 
		
		int x = e.getX();
		int xmove = x-constX;
		int y = e.getY();
		int ymove = y-constY;
				
		dessin.rotationX(xmove);
		dessin.rotationY(ymove);
		
		constY = y;
		constX = x;
	}*/

}
