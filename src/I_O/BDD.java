package I_O;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author RYU
 */
public class BDD {
    
    protected String emplacement;
    protected Input i;
    protected ArrayList<String> nom;
    protected ArrayList<String> type;
    protected ArrayList<String> description;
    protected ArrayList<String> emplac;
    
    public BDD(Input i){
        emplacement = i.emplacement;
        this.i=i;
        
         nom = new ArrayList<>();
      type= new ArrayList<>();
      description = new ArrayList<>();
      emplac = new ArrayList<>();
    }
    
    public BDD(){
         nom = new ArrayList<>();
      type= new ArrayList<>();
      description = new ArrayList<>();
      emplac = new ArrayList<>();
        
    }
    
    
    public void enregistrement(String nom, String description, String type) throws IOException, ClassNotFoundException, SQLException{
        Output out = new Output(i,nom+".gts");
        out.write();
          Connection con=null;
          int nbLigne=0;
      Statement stmt;
      try{
      
      Class.forName("org.sqlite.JDBC"); 
      String url = "jdbc:sqlite:test.sqlite";
      con = DriverManager.getConnection(url);
      stmt = con.createStatement();
      String query = "SELECT COUNT(*) FROM GTS;";
      ResultSet rs = stmt.executeQuery(query);
      nbLigne=rs.getInt(1);
      }catch(Exception e){
        e.getMessage();
      }
      finally{
        con.close();
        }
      try{
      
      Class.forName("org.sqlite.JDBC"); 
      String url = "jdbc:sqlite:test.sqlite";
      con = DriverManager.getConnection(url);
      stmt = con.createStatement();
      String query = "INSERT INTO GTS VALUES('"+nbLigne+"','"+nom+"','"+description+"','"+nom+".gts','"+type+"');";
      ResultSet rs = stmt.executeQuery(query);
      }catch(Exception e){
          e.getMessage();
      }
      finally{
          con.close();
      }
        
    }
    
    public void select() throws ClassNotFoundException, SQLException{
        Connection con=null;
      Statement stmt;
      try{
      
      Class.forName("org.sqlite.JDBC"); 
      String url = "jdbc:sqlite:test.sqlite";
      con = DriverManager.getConnection(url);
      stmt = con.createStatement();
      String query = "SELECT nom, descriptif, type, emplacement FROM GTS;";
      ResultSet rs = stmt.executeQuery(query);
          
      System.out.println("Liste des fichiers dans la base:");
      System.out.println("nom description");
      nom.clear();
      type.clear();
      description.clear();
      emplac.clear();
      while (rs.next()) 
      {
          nom.add(rs.getString(1));
          type.add(rs.getString(3));
          description.add(rs.getString(2));
          emplac.add(rs.getString(4));
              String n = rs.getString(1); // nom
              String p = rs.getString(2); // descriptif
              System.out.println(n + " " + p);
      }
    }catch(ClassNotFoundException | SQLException e){
        e.getMessage();
    }
    finally{
          try{
            con.close();
          }catch(ExceptionInInitializerError e){
              e.getMessage();
          }
    }
    }
    
    public void selectMotCle(String mot) throws SQLException, ClassNotFoundException{
          Connection con=null;
      Statement stmt;
        String[] split = mot.split(" ");
        if(split.length==0){
            select();
        }
        else{
        
            try{
      
      Class.forName("org.sqlite.JDBC"); 
      String url = "jdbc:sqlite:test.sqlite";
      con = DriverManager.getConnection(url);
      stmt = con.createStatement();
      String query = "SELECT nom, descriptif, type, emplacement FROM GTS where idGts like '"+split[0]+"' or nom like '"+split[0]+"' or descriptif like '"+split[0]+"' or type like '"+split[0]+"'";
      
      for(int i=1;i<split.length;i++)
          query+="and idGts like '"+split[i]+"' or nom like ' "+split[i]+"' or descriptif like '"+split[i]+"' or type like '"+split[i]+"'";
      
      query+=";";
      ResultSet rs = stmt.executeQuery(query);
          
      System.out.println("Liste des fichiers dans la base:");
      System.out.println("nom description");
      nom.clear();
      type.clear();
      description.clear();
      emplac.clear();
      while (rs.next()) 
      {
          nom.add(rs.getString(1));
          type.add(rs.getString(3));
          description.add(rs.getString(2));
          emplac.add(rs.getString(4));
              String n = rs.getString(1); // nom
              String p = rs.getString(2); // descriptif
              System.out.println(nom);
      }
    }catch(ClassNotFoundException | SQLException e){
        e.getMessage();
    }
    finally{
          try{
            con.close();
          }catch(ExceptionInInitializerError e){
              e.getMessage();
          }
    }
    }
    }
    
    public ArrayList<String> getNom(){
        return nom;
    }
    
    public ArrayList<String> getType(){
        return type;
    }
    
    public ArrayList<String> getDescription(){
        return description;
    }
    
    public ArrayList<String> getEmplacement(){
        return emplac;
    }
    
    
    
}
