package Coordonnes;

import java.util.ArrayList;

public class Triangle {

	protected Segment s1;
	protected Segment s2;
	protected Segment s3;
	
	protected Point p1;
	protected Point p2;
	protected Point p3;

	public Triangle(Point s, Point d, Point f) {
		this.s1 = new Segment(s, d);
		this.s2 = new Segment(d, f);
		this.s3 = new Segment(f, s);
		
		this.p1 = s;
		this.p2 = d;
		this.p3 = f;
	}
	
	public Triangle(Matrice m) {
		this.p1 = m.getPointX();
		this.p2 = m.getPointY();
		this.p3= m.getPointZ();
	}
	public int[] getTabX(){
		int[] tab = new int[3];
		tab[0] =(int)(p1.getCoord().getX());
		tab[1] = (int)(p2.getCoord().getX());
		tab[2] = (int)(p3.getCoord().getX());
		

		return tab;
	}
	public int[] getTabY(){
		return new int[]{(int)p1.coord.getY(),(int)p2.coord.getY(),(int)p3.coord.getY()};
	}
	public static Point getCentreFigure( ArrayList<Triangle> l){
		Coord retour;
		double x =0 ,y= 0,z =0;
		for(int i=0 ; i<l.size();i++){
			retour = l.get(i).baryCentre();
			x = x+retour.getX();
			y= y+retour.getY();
			z= z+retour.getZ();
		}
		x = x/l.size();
		y = y/l.size();
		z = z/l.size();
		retour = new Coord(x, y, z);
			
		
		
		return new Point(retour);
	}
	
	
	public Point getP1(){
		return this.p1;
	}
	public Point getP2(){
		return this.p2;
	}
	public Point getP3(){
		return this.p3;
	}
        
        public Triangle(Segment s1, Segment s2, Segment s3){
            this.s1=s1;
            this.s2=s2;
            this.s3=s3;
            this.p1 = s1.getP1();
    		this.p2 = s1.getP2();
    		if (!(p1.equals(s2.getP1()))) {
    			this.p3 = s2.getP1();
    		}
    		else{
    			this.p3 = s2.getP2();
    		}
        }
        
        public Matrice getMatrice(){
			return new Matrice(p1,p2,p3);
        	
        }

	public Segment getS1() {
		return s1;
	}

	public void setS1(Segment s1) {
		this.s1 = s1;
	}

	public Segment getS2() {
		return s2;
	}

	public void setS2(Segment s2) {
		this.s2 = s2;
	}

	public Segment getS3() {
		return s3;
	}

	public void setS3(Segment s3) {
		this.s3 = s3;
	}
	
	public Point getPointX(){
		return this.p1;
	}
	
	public Point getPointY(){
		return this.p2;
	}
	
	public Point getPointZ(){
		return this.p3;
	}
        
        public String toString(){
            return "["+p1.toString()+", "+p2.toString()+", "+p3.toString()+"]";
        }

	public Coord baryCentre(){
		double x = (p1.coord.getX()+p2.coord.getX()+p3.coord.getX())/3;
		double y = (p1.coord.getY()+p2.coord.getY()+p3.coord.getY())/3;
		double z = (p1.coord.getZ()+p2.coord.getZ()+p3.coord.getZ())/3;

		return  new Coord(x, y, z);
	}
	


}
