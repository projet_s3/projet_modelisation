
package IHM;

/**
 *
 * @author ALEXIS
 */

import I_O.BDD;
import I_O.Input;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class FenetreEnreBase extends JFrame implements ActionListener {
    
    JButton valider;
    JButton annuler;
    
    JLabel nom;
    JLabel type;
    JLabel description;
    
    JTextField nomText;
    JTextField typeText;
    JTextField descriptionText;
    
    Input i;
    BDD b;
   
    
    public FenetreEnreBase(BDD b){
        setLayout(new GridBagLayout());
        GridBagConstraints g = new GridBagConstraints();
        setSize(500, 200);
        g.insets=new Insets(0, 10, 0, 10);
      
        
        nom = new JLabel("nom");
        g.gridx=0;
        g.gridy=0;
        add(nom,g);
        
        type = new JLabel("type");
        g.gridx=1;
        add(type,g);
        
        description= new JLabel("description");
        g.gridx=2;
        add(description,g);
        
        
        g.ipadx=50;
        nomText = new JTextField();
        g.gridx=0;
        g.gridy=1;
        add(nomText,g);
        
        typeText= new JTextField();
        g.gridx=1;
        add(typeText,g);
        
        descriptionText = new JTextField();
        g.gridx=2;
        add(descriptionText,g);
        
        g.ipadx=0;
        
        valider = new JButton("valider");
        valider.addActionListener(this);
        g.gridx=3;
        g.gridy=2;
        add(valider,g);
        
        annuler = new JButton("annuler");
        annuler.addActionListener(this);
        g.gridx=2;
        add(annuler,g);
        
        this.b=b;
        
       
        
        
        
        
        setVisible(true);
    }
    

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource().equals(valider)){
            
            try {
                b.enregistrement(nomText.getText(), descriptionText.getText(), typeText.getText());
            } catch (Exception e) {
                e.getMessage();
            } 
            this.dispose();
            
            this.setVisible(false);
        }
        else if(ae.getSource().equals(annuler)){
            this.dispose();
        }
    }
    
    
}
